/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx       = 2; /* border pixel of windows */
static const unsigned int snap           = 8; /* snap pixel */
static const unsigned int systrayonleft  = 0; /* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systraypinning = 0; /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2; /* systray spacing */
static const int barfontoffset           = 6; /* Font height offset */
static const int showbar                 = 1; /* 0 means no bar */
static const int showsystray             = 1; /* 0 means no systray */
static const int swallowfloating         = 0; /* 1 means swallow floating windows by default */
static const int systraypinningfailfirst = 1; /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int topbar                  = 0; /* 0 means bottom bar */
static const char *fonts[]     = { "Hermit:size=12", "Iosevka:size=12", "Symbola:size=10", "monospace:size=10", "sans:size=12" };
static const char col_fg[]     = "#ffffff";
static const char col_bg[]     = "#000000";
static const char col_fg_alt[] = "#cccccc";
static const char col_bg_alt[] = "#222222";
static const char *colors[][4] = {
/*               fg          bg          border      float */
[SchemeNorm] = { col_fg_alt, col_bg_alt, col_bg_alt, col_bg_alt },
[SchemeSel]  = { col_fg,     col_bg,     col_fg,     col_fg_alt },
};

static const char term[] = "st";

typedef struct {
    const char *name;
    const void *cmd;
} Sp;

static const char *spcmd1[] = { term, "-n", "scratchpad/term", NULL };
static const char *spcmd2[] = { term, "-n", "scratchpad/calc", "-e", "R", "-q", "--no-save", "--no-restore", NULL };
static const char *spcmd3[] = { term, "-n", "scratchpad/htop", "-e", "htop", NULL };
static const char *spcmd4[] = { term, "-n", "scratchpad/music", "-e", "ncmpcpp", NULL };
static Sp scratchpads[]     = {
/* name               cmd  */
{ "scratchpad/term",  spcmd1 },
{ "scratchpad/calc",  spcmd2 },
{ "scratchpad/htop",  spcmd3 },
{ "scratchpad/music", spcmd4 },
};

/* tagging */
static const unsigned int ntags = 5; /* number of tags that are always shown */
static const char *tags[]       = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };

static const Rule rules[]       = {
/** xprop(1):
 *   WM_CLASS(STRING) = instance, class
 *   WM_NAME(STRING) = title
 */
/* class         instance            title           tags mask  isfloating  isterminal  noswallow  monitor */
{ NULL,          NULL,               "Event Tester", 0,         1,          0,          1,         -1 }, /* xev */

{ "pinentry-qt", NULL,               NULL,           0,         1,          0,          -1,        -1 },
{ "pinentry-gtk-2", NULL,            NULL,           0,         1,          0,          -1,        -1 },

{ "st",          NULL,               NULL,           0,         0,          1,          -1,        -1 },
{ "st-256color", NULL,               NULL,           0,         0,          1,          -1,        -1 },

{ NULL,          NULL,              "OpenSnitch",    OTAGMASK,  1,          0,          -1,        -1 },
{ NULL,       NULL, "OpenSnitch Network Statistics", 1<<4,      0,          0,          -1,        -1 },

{ "Lutris",      NULL,               NULL,           1<<4,      0,          0,          -1,        0 },
{ "Wine",        NULL,               NULL,           1<<4,      0,          0,          -1,        1 },
{ "explorer.exe", NULL,              NULL,           1<<4,      0,          0,          -1,        1 },

{ "Ardour",      NULL,               NULL,           1<<8,      0,          0,          -1,        0 },
{ "Cadence",     NULL,               NULL,           1<<8,      0,          0,          -1,        1 },
{ "Catia",       NULL,               NULL,           1<<8,      0,          0,          -1,        1 },
{ "QjackCtl",    NULL,               NULL,           1<<8,      1,          0,          -1,        1 },
{ "QjackCtl",    NULL,               "Patchbay",     1<<8,      0,          0,          -1,        1 },

{ NULL,          "scratchpad/term",  NULL,           SPTAG(0),  1,          0,          -1,        -1 },
{ NULL,          "scratchpad/calc",  NULL,           SPTAG(1),  1,          0,          -1,        -1 },
{ NULL,          "scratchpad/htop",  NULL,           SPTAG(2),  0,          0,          -1,        -1 },
{ NULL,          "scratchpad/music", NULL,           SPTAG(3),  1,          0,          -1,        -1 },
};

/* layout(s) */
static const float mfact        = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster        = 1;    /* number of clients in master area */
static const int resizehints    = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0;    /* 1 will force focus on the fullscreen window */

#include "fibonacci.c"
static const Layout layouts[] = {
/* symbol     arrange function */
{ "[=]",      tile },    /* first entry is default */
{ "/~/",      NULL },    /* no layout function means floating behavior */
{ "[M]",      monocle },
{ "TTT",      bstack },
{ "===",      bstackhoriz },
{ "[@]",      spiral },
{ "[\\]",     dwindle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG)                                                \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }

#define EXECCMD(...) { .v = (const char*[]){ __VA_ARGS__, NULL } }
/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
/* #include <X11/XF86keysym.h> */
/* static const char audioctl[]      = "volume.sh"; */
/* static const char brightnessctl[] =  "backlight.sh"; */
static const char screenshotctl[] = "screenshot.sh";

static const char *termcmd[]              = { term, NULL };
static const char *browsercmd[]           = { "firefox", NULL };
static const char *altbrowsercmd[]        = { "chromium", NULL };
static const char *privatebrowsercmd[]    = { "torbrowser", "--allow-remote", NULL };
static const char *privatealtbrowsercmd[] = { "chromium", "--incognito", NULL };
static const char *editorcmd[]            = { "emacs", NULL };


static Key keys[] = {
/* modifier                     key        function        argument */
{ MODKEY,                       XK_space,  spawn,          EXECCMD( "dmenu_run" ) },
{ MODKEY,                       XK_Return, spawn,          {.v = termcmd } },
{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = editorcmd } },
{ MODKEY,                       XK_q,      killclient,     {0} },
{ MODKEY|ShiftMask,             XK_q,      spawn,          EXECCMD( "xkill" ) },
{ MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {0} },
{ MODKEY,                       XK_b,      togglebar,      {0} },

/* Stack */
{ MODKEY,                       XK_h,      incnmaster,     {.i = +1 } },
{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
{ MODKEY,                       XK_l,      incnmaster,     {.i = -1 } },

{ MODKEY|ShiftMask,             XK_h,      setmfact,       {.f = -0.05} },
{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
{ MODKEY|ShiftMask,             XK_l,      setmfact,       {.f = +0.05} },

/* Window management */
{ MODKEY,                       XK_r,      resetlayout,    {0} },
{ MODKEY,                       XK_grave,  zoom,           {0} },
{ MODKEY,                       XK_Tab,    view,           {0} },
{ MODKEY,                       XK_e,      setlayout,      {.v = &layouts[0]} }, /* tile */
{ MODKEY,                       XK_w,      setlayout,      {.v = &layouts[1]} }, /* float */
{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[2]} }, /* monocle */
{ MODKEY|ShiftMask,             XK_e,      setlayout,      {.v = &layouts[3]} }, /* bstack */
{ MODKEY,                       XK_d,      setlayout,      {.v = &layouts[6]} }, /* dwindle */
{ MODKEY|ShiftMask,             XK_f,      togglefloating, {0} },

{ MODKEY,                       XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
{ MODKEY,                       XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
{ MODKEY,                       XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
{ MODKEY,                       XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
{ MODKEY|ShiftMask,             XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
{ MODKEY|ShiftMask,             XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
{ MODKEY|ShiftMask,             XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
{ MODKEY|ShiftMask,             XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },
{ MODKEY|ControlMask,           XK_Up,     moveresizeedge, {.v = "t"} },
{ MODKEY|ControlMask,           XK_Down,   moveresizeedge, {.v = "b"} },
{ MODKEY|ControlMask,           XK_Left,   moveresizeedge, {.v = "l"} },
{ MODKEY|ControlMask,           XK_Right,  moveresizeedge, {.v = "r"} },
{ MODKEY|ControlMask|ShiftMask, XK_Up,     moveresizeedge, {.v = "T"} },
{ MODKEY|ControlMask|ShiftMask, XK_Down,   moveresizeedge, {.v = "B"} },
{ MODKEY|ControlMask|ShiftMask, XK_Left,   moveresizeedge, {.v = "L"} },
{ MODKEY|ControlMask|ShiftMask, XK_Right,  moveresizeedge, {.v = "R"} },

{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },

{ MODKEY,                       XK_g,      spawn,          {.v = browsercmd } },
{ MODKEY|ControlMask,           XK_g,      spawn,          {.v = altbrowsercmd } },
{ MODKEY|ShiftMask,             XK_g,      spawn,          {.v = privatebrowsercmd } },
{ MODKEY|ControlMask|ShiftMask, XK_g,      spawn,          {.v = privatealtbrowsercmd } },
{ MODKEY,                       XK_s,      spawn,          EXECCMD( "signal-desktop", "--use-tray-icon" ) },
{ MODKEY,                       XK_y,      togglescratch,  {.ui = 0 } }, /* term */
{ MODKEY,                       XK_c,      togglescratch,  {.ui = 1 } }, /* calc */
{ MODKEY,                       XK_i,      togglescratch,  {.ui = 2 } }, /* htop */
{ MODKEY,                       XK_m,      togglescratch,  {.ui = 3 } }, /* music */

/* Dunst */
{ MODKEY|ShiftMask,             XK_space,  spawn,          EXECCMD( "dunstctl", "history-pop" ) },
{ MODKEY|ControlMask,           XK_space,  spawn,          EXECCMD( "dunstctl", "close" ) },
{ MODKEY|ControlMask|ShiftMask, XK_space,  spawn,          EXECCMD( "dunstctl", "context" ) },

/* Pass */
{ MODKEY,                       XK_p,      spawn,          EXECCMD( "dmenu_pass", "type", "password" ) },
{ MODKEY|ControlMask,           XK_p,      spawn,          EXECCMD( "dmenu_pass", "type", "otp" ) },
{ MODKEY|ShiftMask,             XK_p,      spawn,          EXECCMD( "dmenu_pass", "type", "username" ) },
{ MODKEY|ControlMask|ShiftMask, XK_p,      spawn,          EXECCMD( "dmenu_pass" ) },

/* Screenshot */
/**
 * Modkey:        clipboard
 * Shift:         whole screen
 * Control:       current window
 * Control+Shift: selection
 */
{ 0,                            XK_Print,  spawn,          EXECCMD( screenshotctl ) },
{ ShiftMask,                    XK_Print,  spawn,          EXECCMD( screenshotctl, "file", "screen" ) },
{ ControlMask,                  XK_Print,  spawn,          EXECCMD( screenshotctl, "file", "window" ) },
{ ControlMask|ShiftMask,        XK_Print,  spawn,          EXECCMD( screenshotctl, "file", "area" ) },
{ MODKEY,                       XK_Print,  spawn,          EXECCMD( screenshotctl, "clip" ) },
{ MODKEY|ShiftMask,             XK_Print,  spawn,          EXECCMD( screenshotctl, "clip", "screen" ) },
{ MODKEY|ControlMask,           XK_Print,  spawn,          EXECCMD( screenshotctl, "clip", "window" ) },
{ MODKEY|ControlMask|ShiftMask, XK_Print,  spawn,          EXECCMD( screenshotctl, "clip", "area" ) },

/* Special keys */
/* { 0,             XF86XK_AudioLowerVolume,  spawn,          EXECCMD( audioctl, "dec", "5" ) }, */
/* { 0,             XF86XK_AudioRaiseVolume,  spawn,          EXECCMD( audioctl, "inc", "5" ) }, */
/* { ShiftMask,     XF86XK_AudioLowerVolume,  spawn,          EXECCMD( audioctl, "dec", "10" ) }, */
/* { ShiftMask,     XF86XK_AudioRaiseVolume,  spawn,          EXECCMD( audioctl, "inc", "10" ) }, */
/* { ShiftMask,            XF86XK_AudioMute,  spawn,          EXECCMD( audioctl, "mute", "toggle" ) }, */

/* { 0,            XF86XK_MonBrightnessDown,  spawn,          EXECCMD( brightnessctl, "dec", "5" ) }, */
/* { 0,              XF86XK_MonBrightnessUp,  spawn,          EXECCMD( brightnessctl, "inc", "5" ) }, */
/* { ShiftMask,    XF86XK_MonBrightnessDown,  spawn,          EXECCMD( brightnessctl, "dec", "10" ) }, */
/* { ShiftMask,      XF86XK_MonBrightnessUp,  spawn,          EXECCMD( brightnessctl, "inc", "10" ) }, */

/* { 0,            XF86XK_KbdBrightnessDown,  spawn,          EXECCMD( brightnessctl, "kbd", "dec", "25" ) }, */
/* { 0,              XF86XK_KbdBrightnessUp,  spawn,          EXECCMD( brightnessctl, "kbd", "inc", "25" ) }, */

{ MODKEY,                       XK_0,      view,           {.ui = OTAGMASK } },
{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = OTAGMASK } },
TAGKEYS(                        XK_1,                      0),
TAGKEYS(                        XK_2,                      1),
TAGKEYS(                        XK_3,                      2),
TAGKEYS(                        XK_4,                      3),
TAGKEYS(                        XK_5,                      4),
TAGKEYS(                        XK_6,                      5),
TAGKEYS(                        XK_7,                      6),
TAGKEYS(                        XK_8,                      7),
TAGKEYS(                        XK_9,                      8),
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
/* click         event mask  button   function        argument */
{ ClkLtSymbol,   0,          Button1, setlayout,      {0} },
{ ClkLtSymbol,   0,          Button3, resetlayout,    {0} },
{ ClkWinTitle,   0,          Button2, zoom,           {0} },
{ ClkStatusText, 0,          Button2, spawn,          {.v = termcmd } },
{ ClkClientWin,  MODKEY,     Button1, movemouse,      {0} },
{ ClkClientWin,  MODKEY,     Button2, togglefloating, {0} },
{ ClkClientWin,  MODKEY,     Button3, resizemouse,    {0} },
{ ClkTagBar,     0,          Button1, view,           {0} },
{ ClkTagBar,     0,          Button3, toggleview,     {0} },
{ ClkTagBar,     MODKEY,     Button1, tag,            {0} },
{ ClkTagBar,     MODKEY,     Button3, toggletag,      {0} },
};
